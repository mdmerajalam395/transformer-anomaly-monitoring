/** @file wifi.cpp
 *  @brief This program configures the wifi connectivity
 *  Here, a JSON configuration file(config.json), with necessary configuration for wifi connectivity (i.e. a static ip address is provided) is parsed using rapidJSON library
 *  @Janani B
 */

/* --- Standard Includes --- */
#include <iostream>
#include <fstream>
#include <string>

/* --- Includes for using rapidJSON library to parse JSON --- */
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <rapidjson/ostreamwrapper.h>

using namespace rapidjson;
using namespace std;

/** 
 *  @brief parseJSON function parses the json file and returns the value for given key.
 *  First the config.json file is opened
 *  Then using rapidJSON library element 'Document' it is parsed.
 *  @return value for the keys(the parameters for the ethernet configuration)
 */
string parseJSON(string key1, string key2)//Function returns the value from the JSON file according to the keys provided
{	
	ifstream conf("./../Configuration JSON/config.json");//open the json file
	IStreamWrapper json(conf);//store the json file
	Document d;
	d.ParseStream(json); // parse the json file
	return(d[key1.c_str()][key2.c_str()].GetString());//return the value string for the key
}

/** 
 *  @brief configure function uses the json values and write into the wifi configuration file.
 */


void configure(void)//configures wifi connectivity
{
	
    // open a file in append, write and read mode.
	FILE *fp;
	fp = fopen("/etc/wpa_supplicant/wpa_supplicant.conf", "w");
	if(fp == NULL)//prints "Error!" if fopen returns "null" due to any problem.
	{
	    printf("Error!");   
		exit(1);    
	}
	fp = freopen("/etc/wpa_supplicant/wpa_supplicant.conf", "w+", stdout);//it erases all the previous data from the file

    /* --- Write into the .conf file --- */
	fprintf(fp,"network ={\n");
    fprintf(fp,"ssid=%s\n", parseJSON("wireless","ssid").c_str());
    fprintf(fp,"psk=%s\n}\n", parseJSON("wireless","psk").c_str());
	fclose(fp);//close the file
}

/*
*  main() function calls the configure() function to run and execute code
*/
int main(void)
{
	configure();//calls the function
	return(0);//returns 0 if program runs successfully.
}

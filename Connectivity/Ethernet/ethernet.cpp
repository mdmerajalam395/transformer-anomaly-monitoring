/** @file ethernet.cpp
 *  @brief This program configures the ethernet connectivity
 * 
 *  Here, a JSON configuration file(config.json),
 *  with necessary configuration for ethernet connectivity 
 *  (i.e. a static ip address is provided) is parsed using rapidJSON library.
 *  The values of the JSON file are written in the /etc/dhcpcd.conf file to
 *  provide a static ip address to the module.
 * 
 *  @author Subhadananda Dash
 *  @bug  It should be run with root privileges (just like 'sudo' in Ubuntu)
 *
 */

/* --- Standard Includes --- */
#include <iostream>
#include <fstream>
#include <string>

/* --- Includes for using rapidJSON library to parse JSON --- */
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <rapidjson/ostreamwrapper.h>

using namespace rapidjson;
using namespace std;

/** 
 *  @brief parseJSON function parses the json file and returns the value for given key.
 *  
 *  First the config.json file is opened
 *  Then using rapidJSON library element 'Document' it is parsed.
 *  
 *  @return value for the keys(the parameters for the ethernet configuration)
 */
string parseJSON(string key1, string key2)//Function returns the value from the JSON file according to the keys provided
{	
	ifstream conf("./../Configuration JSON/config.json");//open the json file
	IStreamWrapper json(conf);//store the json file
	Document d;
	d.ParseStream(json); // parse the json file
	return(d[key1.c_str()][key2.c_str()].GetString());//return the value string for the key
}



/** 
 *  @brief configure function uses the json values
 *  and write into the ethernet configuration file.
 *  
 *  Open the /etc/dhcpcd.conf file and write the necessary configuration
 *  by using values got from the parsed json.
 *  
 */


void configure(void)//configures ethernet connectivity
{
	
    // open a file in append, write and read mode.
	FILE *fp;
	fp = fopen("/etc/dhcpcd.conf", "w");
	if(fp == NULL)//prints "Error!" if fopen returns "null" due to any problem.
	{
	    printf("Error!");   
		exit(1);    
	}
	fp = freopen("/etc/dhcpcd.conf", "w+", stdout);//it erases all the previous data from the file

    /* --- Write into the .conf file --- */
	fprintf(fp,"interface %s\n", parseJSON("ethernet","device").c_str());
    fprintf(fp,"static ip_address=%s\n", parseJSON("ethernet","ipAddress").c_str());
    fprintf(fp,"static routers=%s\n", parseJSON("ethernet","routerAddress").c_str());
    fprintf(fp,"static netmask=%s\n", parseJSON("ethernet","subnetMask").c_str());
	fprintf(fp,"static domain_name_servers=%s\n", parseJSON("ethernet","dnsAddress").c_str());
	fclose(fp);//close the file
}

/*
*  main() function calls the configure() function to run and execute code
*/
int main(void)
{
	configure();//calls the function
	return(0);//returns 0 if program runs successfully.
}
